---
title: Hello World
date: 2021/1/17 11:25:30
updated: 2021/1/17 19:31:49
excerpt: 此 Blog 搭建过程记录（保持更新）
---

## 用 [Hexo](https://hexo.io) 搭建的博客

好处不用说了，比如可以直接写 `Markdown` 等等，搜索 `Hexo` 会得到一大把相关说明甚至教程。

`Hello World` 就用来存这个 blog 的搭建过程和基本配置吧。

如果后续有工具或组件的更新，也会写在这篇内容里…… 


### 基础搭建

> 确认 `node.js` 和 `yarn` 已经准备好

```console
$ npm i -g hexo-cli
$ npm init blog
$ cd blog
$ yarn

## 自动显示字数和阅读时间
$ yarn add hexo-wordcount

## 自动在中文和英文之间加入空格
$ yarn add hexo-filter-auto-spacing

## 自动摘要（我没装，感觉我选的主题不装它更美观，有需要的同学自己动手吧）
$ yarn add hexo-auto-excerpt

## 自动实时预览
$ npm i -g browser-sync
$ yarn add --dev hexo-browsersync
```

### 安装 Acetolog 主题并设置图标和头像

> Acetolog: https://github.com/iGuan7u/Acetolog

```console
$ yarn add hexo-renderer-swig
$ cd themes
$ git clone https://github.com/iGuan7u/Acetolog
```

**两个图标文件：**

- `favicon.ico`
- `apple-touch-icon.png`

生成之后存在主题资源文件夹 `/themes/Acetolog/source` 里

**gravatar 的在线头像**

粘在 `/themes/Acetolog/_config.yml` 的 `avatar` 配置后边

```yaml
avatar: https://secure.gravatar.com/avatar/4927b9863674cda74b85dbce5acc6ab6?s=800&d=identicon
```


### 设置整站 `_config.yml` 配置信息

```yaml
title: 12345.blog
subtitle: Neo Smith's blog
description: Find something interesting or valuable...
bio: Valuable or interesting
keywords: Neo Smith NeoSmith
author: Neo Smith
language: zh_CN
timezone: Asia/Shanghai

url: https://12345.blog
permalink: :year/:title/  # 永久链接格式
auto_spacing: true  # 自动在中文和英文之间加入空格

theme: Acetolog  # 使用 Acetolog 主题
highlight:
  enable: true
  hljs: true  # 开启代码高亮

# 其它的要么不必要修改，要么还不知道是啥，就这样吧
```


## 可以自动构建并发布到 GitHub Pages

按步骤抄作业 https://hexo.io/zh-cn/docs/github-pages

对应的 `.travis.yml` 如下：

```yaml
sudo: false
language: node_js
node_js:
  - lts/*
cache: npm
branches:
  only:
    - master
script:
  - hexo generate
deploy:
  provider: pages
  skip-cleanup: true
  github-token: $GH_TOKEN
  keep-history: true
  on:
    branch: master
  local-dir: public
```

## 然而我放在了 GitLab Pages 上

只是因为它可以带 `https` 使用自己的域名，而且貌似 CI 更简单

还是抄 [Hexo](https://hexo.io) 官方作业 https://hexo.io/zh-cn/docs/gitlab-pages

对应的 `.gitlab-ci.yml` 如下：

```yaml
image: node:10-alpine # use nodejs v10 LTS
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```
